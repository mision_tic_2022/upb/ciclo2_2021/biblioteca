public class Libro {
    //Constante
    private static final String ISBN = "COL";

    /************
     * Atributos
     ************/
    private String codigo_libro;
    private String nombre;
    private Autor autor;

    //Constructores
    public Libro(Autor autor, String nombre){
        this.autor = autor;
        this.nombre = nombre;
    }

    public Libro(Autor autor, String nombre, String codigo_libro){
        this.autor = autor;
        this.nombre = nombre;
        this.codigo_libro = codigo_libro;
    }

    /************
     * Consultores
     * (Getters)
     ************/

    public static String getIsbn() {
        return ISBN;
    }

    public String getCodigo_libro() {
        return codigo_libro;
    }

    public String getNombre() {
        return nombre;
    }

    public Autor getAutor() {
        return autor;
    }

    /************
     * Modificadores
     * (Setters)
     ************/

    public void setCodigo_libro(String codigo_libro) {
        this.codigo_libro = codigo_libro;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    
    
}
