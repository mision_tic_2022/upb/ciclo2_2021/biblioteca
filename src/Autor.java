public class Autor {
    /************
     * Atributos
     ************/
    private String nombre;
    private String apellido;
    private String nacionalidad;
    private char genero;
    private Libro[] libros;

    public Autor(String nombre, String apellido, String nacionalidad, char genero){
        this.nombre = nombre;
        this.apellido = apellido;
        this.nacionalidad = nacionalidad;
        this.genero = genero;
        this.libros = new Libro[5];
    }

    public Autor(){
        this.libros = new Libro[5];
    }

    /************
     * Consultores
     * (Getters)
     ************/

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public char getGenero() {
        return genero;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Libro[] getLibros() {
        return libros;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public void setLibros(Libro[] libros) {
        this.libros = libros;
    }

    public void crear_libro(String nombre){
        Libro objLibro = new Libro(this, nombre);
        
        for(int i = 0; i < this.libros.length; i++){
            if(this.libros[i].equals(null)){

            }
        }
    }
}
